package com.gmail.theposhogamer.Commands;

import com.gmail.theposhogamer.RandomTP;
import com.gmail.theposhogamer.Utils.TeleportTask;
import java.util.UUID;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

public class Cmd implements CommandExecutor {
   private RandomTP plugin;

   public Cmd(RandomTP plugin) {
      this.plugin = plugin;
   }

   public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
      if (cmd.getName().equalsIgnoreCase("rtp")) {
         if (args.length == 0) {
            if (sender.hasPermission("randomtp.basic")) {
               PluginDescriptionFile pdf = this.plugin.getDescription();
               sender.sendMessage(" ");
               sender.sendMessage("§6§lRandomTP - §e§l" + pdf.getVersion() + " §7§lBuilt:" + this.plugin.state + " §6§l- §7command list:");
               sender.sendMessage("§f§ §e/rtp tp <player> <world> <distance> | random teleports the mentioned player to a random location with the mentioned setting values");
               sender.sendMessage("§f§ §e/rtp list | shows you a list of the randomtp sign placed locations");
               sender.sendMessage("§f§ §e/rtp gui | opens you a randomtp menu");
               sender.sendMessage("§f§ §e/rtp setcooldown | type this and then click a randomtp sign to add a cooldown to it");
               sender.sendMessage("§f§ §e/rtp pcreator | opens the portal creator menu");
               sender.sendMessage("§f§ §e/rtp version | shows you the randomtp current version");
               sender.sendMessage("§f§ §e/rtp backup | backups database information, like cooldowns and more");
            } else {
               this.sendNoPermissionMessage(sender);
            }

            return true;
         }

         String args0 = args[0];
         String args1;
         if (args0.equalsIgnoreCase("tp")) {
            if (sender.hasPermission("randomtp.tp")) {
               args1 = this.plugin.getPrefix();
               if (args.length != 4) {
                  sender.sendMessage(args1 + "§eUsage /rtp tp player world distance");
               } else {
                  Player p = this.plugin.getServer().getPlayer(args[1]);
                  if (p == null) {
                     sender.sendMessage(args1 + "§cCouldn't find player");
                     return true;
                  }

                  World w = this.plugin.getServer().getWorld(args[2]);
                  if (w == null) {
                     sender.sendMessage(args1 + "§cCouldn't find world");
                     return true;
                  }

                  String distance = args[3];
                  if (!this.isInteger(distance)) {
                     sender.sendMessage(args1 + "§cPlease introduce a number '" + distance + "' is not a number");
                     return true;
                  }

                  int distanceint = Integer.valueOf(distance);
                  sender.sendMessage(args1 + "§bTeleporting " + p.getName() + " to a random location in the world: " + w.getName() + " to a distance range of " + distanceint + " block/s");
                  this.plugin.getPlayersInTeleportProcess().put(p.getUniqueId(), new TeleportTask(p, this.plugin, w.getName(), distanceint));
               }
            } else {
               this.sendNoPermissionMessage(sender);
            }

            return true;
         }

         Player p;
         if (args0.equalsIgnoreCase("pcreator")) {
            if (sender instanceof Player) {
               p = (Player)sender;
               if (p.hasPermission("randomtp.portalcreator")) {
                  this.plugin.getPortalUtils().openPortalCreator(p);
               } else {
                  this.sendNoPermissionMessage(sender);
               }
            }

            return true;
         }

         if (args0.equalsIgnoreCase("list")) {
            if (sender.hasPermission("randomtp.list")) {
               if (args.length != 2) {
                  this.plugin.getHelp().sendMessage(sender, 0);
               } else {
                  args1 = args[1];
                  if (this.plugin.getSignUtil().isNumeric(args1)) {
                     this.plugin.getHelp().sendMessage(sender, Integer.valueOf(args1));
                  } else {
                     sender.sendMessage(this.plugin.getPrefix() + "§c" + args1 + " is not a valid number of page");
                  }
               }
            } else {
               this.sendNoPermissionMessage(sender);
            }

            return true;
         }

         if (args0.equalsIgnoreCase("setcooldown")) {
            if (sender instanceof Player) {
               p = (Player)sender;
               if (p.hasPermission("randomtp.setcooldown")) {
                  if (args.length == 2) {
                     String number = args[1];
                     if (this.plugin.getSignUtil().isNumeric(number)) {
                        int cooldown = Integer.valueOf(number);
                        UUID uuid = p.getUniqueId();
                        this.plugin.getSignUtil().uuidCooldown.put(uuid, cooldown);
                        p.sendMessage(this.plugin.getPrefix() + "§aPlease now click " + "the sign to add the " + number + " seconds cooldown");
                     } else {
                        p.sendMessage(this.plugin.getPrefix() + "§c" + number + " is not a valid number of seconds");
                     }
                  } else {
                     p.sendMessage(this.plugin.getPrefix() + "§cUsage: /rtp setcooldown <number>");
                  }
               } else {
                  this.sendNoPermissionMessage(sender);
               }
            }

            return true;
         }

         if (args0.equalsIgnoreCase("version")) {
            PluginDescriptionFile pdf = this.plugin.getDescription();
            sender.sendMessage(this.plugin.getPrefix() + "§cSpigot plugin coded " + "by AnyOD §6spigotmc.org/resources/authors/34653§c, version " + pdf.getVersion() + ", built:" + this.plugin.state);
            return true;
         }

         if (args0.equalsIgnoreCase("backup")) {
            if (sender.hasPermission("randomtp.backup")) {
               sender.sendMessage(this.plugin.getPrefix() + "§cSaving all data §7(this may cause some lag " + "depending on your amount of players)§c...");
               this.plugin.backup();
               return true;
            }

            this.sendNoPermissionMessage(sender);
            return true;
         }

         if (args0.equalsIgnoreCase("gui")) {
            p = (Player)sender;
            if (p.hasPermission("randomtp.gui")) {
               this.plugin.getGui().openGUI(p);
            } else {
               this.sendNoPermissionMessage(sender);
            }

            return true;
         }
      }

      return true;
   }

   public void sendNoPermissionMessage(CommandSender sender) {
      sender.sendMessage(this.plugin.getLang().getMessage("NOPERMISSION").replace("&", "§"));
   }

   public boolean isInteger(String s) {
      try {
         Integer.parseInt(s);
         return true;
      } catch (NumberFormatException var3) {
         return false;
      }
   }
}
