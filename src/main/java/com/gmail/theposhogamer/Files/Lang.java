package com.gmail.theposhogamer.Files;

import com.gmail.theposhogamer.RandomTP;
import java.io.File;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Lang extends YamlConfiguration {
   private FileConfiguration data;
   private File dfile;

   public FileConfiguration getConfig() {
      return this.data;
   }

   public Lang(RandomTP main) {
      this.dfile = new File(main.getDataFolder(), "lang.yml");
      if (!this.dfile.exists()) {
         try {
            this.dfile.createNewFile();
         } catch (Exception var3) {
            main.getServer().getConsoleSender().sendMessage("An exception ocurred while trying to create a new lang yml file");
         }
      }

      this.data = YamlConfiguration.loadConfiguration(this.dfile);
      this.data.addDefault("NOPERMISSION", "&cYou do not have permission to perform this action");
      this.data.addDefault("NOPERMISSIONGUIUSE", "&eYou do not have permission to use this gui, &apurchase access in our website");
      this.data.addDefault("ECONOMYNOTFOUND", "&cNo economy plugin was detected, so price was set to 0");
      this.data.addDefault("NOTVALIDBLOCKNUMBER", "&c%variable% is not a valid number of distance");
      this.data.addDefault("EMPTYBLOCKNUMBER", "&cDistance cannot be empty");
      this.data.addDefault("NOTVALIDPRICENUMBER", "&c%variable% is not a valid price");
      this.data.addDefault("EMPTYPRICENUMBER", "&cDistance cannot be empty");
      this.data.addDefault("INVALIDWORLD", "&cThat world does not exist");
      this.data.addDefault("UNEXISTENT", "&cInvalid world, cannot teleport randomly");
      this.data.addDefault("CREATINGSUCCESS", "&aSign was successfully created");
      this.data.addDefault("SIGNREMOVED", "&cSign successfully was removed");
      this.data.addDefault("SUCCESSTELEPORT", "&aYou have been teleported to a random location");
      this.data.addDefault("UNSUCCESSFULTELEPORT", "&aNo location found, try again please");
      this.data.addDefault("INSSUFICIENTMONEY", "&cYou don't have enough money to buy this teleport, you need $%variable% to buy this");
      this.data.addDefault("SUCCESSFULBUY", "&aYou have afforded $%variable% to buy this random teleport");
      this.data.addDefault("COOLDOWNLEFT", "&6You need to wait %variable% before using this again");
      this.data.addDefault("CONSOLEINVALIDBLOCKSNUMBER", "&cInvalid blocks distance, have you written a number?");
      this.data.addDefault("SEARCHINGLOCATION", "&aSearching for random location for you, please wait");
      this.data.addDefault("PORTAL.GATEDOESNOTLEADANYWHERE", "&eThis portal does not lead anywhere");
      this.data.addDefault("PORTAL.NOPERMISSION", "&cYou do not have permission to use this portal");
      this.data.addDefault("NOTALLOWEDINTHATWORLD", "&cYou cannot use any kind of Random Teleport in this world");
      this.data.options().copyDefaults(true);
      this.save();
   }

   public String getMessage(String param) {
      return this.getConfig().getString(param);
   }

   public void reload() {
      this.data = YamlConfiguration.loadConfiguration(this.dfile);
   }

   public void save() {
      try {
         this.data.save(this.dfile);
      } catch (Exception var2) {
         System.out.println("[RandomTP] An exception ocurred while trying save the lang yml file");
      }

   }
}
