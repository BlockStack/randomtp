package com.gmail.theposhogamer.Files;

import com.gmail.theposhogamer.RandomTP;
import java.io.File;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

public class Database extends YamlConfiguration {
   private RandomTP plugin;
   private FileConfiguration data;
   private File dfile;

   public FileConfiguration getConfig() {
      return this.data;
   }

   public Database(RandomTP main) {
      this.plugin = main;
      this.dfile = new File(main.getDataFolder(), "database.yml");
      if (!this.dfile.exists()) {
         try {
            this.dfile.createNewFile();
         } catch (Exception var3) {
            System.out.println("[RandomTP] An exception ocurred while trying to create a new database yml file");
         }
      }

      this.data = YamlConfiguration.loadConfiguration(this.dfile);
   }

   public void reload() {
      (new BukkitRunnable() {
         public void run() {
            Database.this.data = YamlConfiguration.loadConfiguration(Database.this.dfile);
         }
      }).runTaskLaterAsynchronously(this.plugin, 0L);
   }

   public void save() {
      (new BukkitRunnable() {
         public void run() {
            try {
               Database.this.data.save(Database.this.dfile);
            } catch (Exception var2) {
               System.out.println("[RandomTP] An exception ocurred while trying save the database yml file");
            }

         }
      }).runTaskLaterAsynchronously(this.plugin, 0L);
   }
}
