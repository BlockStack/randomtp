package com.gmail.theposhogamer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.gmail.theposhogamer.Commands.Cmd;
import com.gmail.theposhogamer.Files.Database;
import com.gmail.theposhogamer.Files.Lang;
import com.gmail.theposhogamer.Listeners.Events;
import com.gmail.theposhogamer.Portals.Portal;
import com.gmail.theposhogamer.Utils.ExternalReferences;
import com.gmail.theposhogamer.Utils.GUI;
import com.gmail.theposhogamer.Utils.PortalUtil;
import com.gmail.theposhogamer.Utils.PrivateInventory;
import com.gmail.theposhogamer.Utils.RMaterial;
import com.gmail.theposhogamer.Utils.ReadableHelp;
import com.gmail.theposhogamer.Utils.SignUtil;
import com.gmail.theposhogamer.Utils.TeleportTask;
import com.gmail.theposhogamer.Utils.UpdateChecker;
import com.gmail.theposhogamer.Utils.VersionUtil;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import net.milkbowl.vault.economy.Economy;

public class RandomTP extends JavaPlugin {
   private VersionUtil.ServerVersion version;
   private Map<UUID, TeleportTask> playersInTeleportProcess = new HashMap<>();
   private Map<UUID, Location> locationsToTeleport = new HashMap<>();
   private Map<Integer, Portal> portals = new HashMap<>();
   private List<String> disabledWorlds = new ArrayList<>();
   private String prefix = "§7(§6§lRandomTP§7) ";
   private Economy economy;
   private Lang langFile;
   private Database databaseFile;
   private SignUtil signUtils;
   private ExternalReferences externalRefs;
   private ReadableHelp help;
   private GUI gui;
   private PortalUtil portalUtils;
   private VersionUtil versionUtil;
   private boolean spawnBoats = false;
   int counter = 300;
   public String state = "§7§lUnknown";

   public List<String> getDisabledWorlds() {
      return this.disabledWorlds;
   }

   public void onEnable() {
      this.getServer().getPluginManager().registerEvents(PrivateInventory.getListener(), this);
      this.loadConfigFiles();
      this.loadConfigPreferences();
      this.registerModules();
      this.loadModules();
      this.registerEvents();
      this.registerCommands();
      this.checkSoftDependencies();
      this.startRunnableTask();
      new UpdateChecker(this, 5084);
      this.sendPluginEnabledMessage();
   }

   public void onDisable() {
      this.log("saving relevant information to the database...");
      this.log("save successful, disabling plugin");
   }

   public void log(String message) {
      this.getServer().getConsoleSender().sendMessage(this.getPrefix() + "§a" + message);
   }

   public void sendPluginEnabledMessage() {
      (new BukkitRunnable() {
         public void run() {
            RandomTP.this.log("plugin adapted to package " + RandomTP.this.versionUtil.getRawVersion());
            RandomTP.this.log("the plugin has been successfully loaded & enabled");
         }
      }).runTaskLater(this, 0L);
   }

   public void backup() {
      SignUtil signUtils = this.getSignUtil();
      signUtils.reloadSigns(false);
      signUtils.saveCooldowns();
      signUtils.saveDatabase();
   }

   public void registerCommands() {
      this.getCommand("rtp").setExecutor(new Cmd(this));
   }

   public void registerEvents() {
      new Events(this);
   }

   public void registerModules() {
      this.versionUtil = new VersionUtil(this);
      this.gui = new GUI(this);
      this.help = new ReadableHelp(this);
      this.signUtils = new SignUtil(this);
      this.externalRefs = new ExternalReferences(this);
      this.portalUtils = new PortalUtil(this);
   }

   public void loadModules() {
      (new BukkitRunnable() {
         public void run() {
            RandomTP.this.version = RandomTP.this.versionUtil.getVersion();
            RandomTP.this.getPortalUtils().loadPortals();
            RandomTP.this.gui.loadGUI();
            RandomTP.this.signUtils.loadSigns(true);
            RandomTP.this.signUtils.loadCooldowns();
            RandomTP.this.help.reload();
            RandomTP.this.portalUtils.startPortalCheckTask();
         }
      }).runTaskLater(this, 0L);
   }

   public void loadConfigFiles() {
      this.getConfig().options().copyDefaults(true);
      this.saveConfig();
      this.databaseFile = new Database(this);
      this.langFile = new Lang(this);
   }

   public void loadConfigPreferences() {
      this.spawnBoats = this.getConfig().getBoolean("SpawnBoatsInWater");
      if (this.getConfig().getBoolean("DisabledInWorlds.enabled")) {
         this.disabledWorlds = this.getConfig().getStringList("DisabledInWorlds.worlds");
      }

      this.counter = 180;
   }

   public void checkSoftDependencies() {
      PluginManager plManager = this.getServer().getPluginManager();
      ExternalReferences extRef = this.getExternalReferences();
      if (plManager.getPlugin("Vault") != null) {
         RegisteredServiceProvider<Economy> service = this.getServer().getServicesManager().getRegistration(Economy.class);
         if (service != null) {
            this.economy = (Economy)service.getProvider();
         }

         extRef.vault = true;
         this.log("Linked with Vault-Economy");
      }

      if (plManager.getPlugin("Factions") != null && plManager.getPlugin("MassiveCore") != null) {
         extRef.factions = true;
         this.log("Linked with Factions");
      }

      if (plManager.getPlugin("WorldBorder") != null) {
         extRef.worldBorder = true;
         this.log("Linked with WorldBorder");
      }

      if (plManager.getPlugin("Residence") != null) {
         extRef.residence = true;
         this.log("Linked with Residence");
      }

      if (plManager.getPlugin("Towny") != null) {
         extRef.towny = true;
         this.log("Linked with Towny");
      }

      if (plManager.getPlugin("WorldGuard") != null && plManager.getPlugin("WorldEdit") != null) {
         extRef.worldguard = true;
         this.externalRefs.wg = this.externalRefs.getWorldGuard();
         this.log("Linked with WorldGuard");
      }

      if (plManager.getPlugin("GriefPrevention") != null) {
         extRef.griefprevention = true;
         this.log("Linked with GriefPrevention");
      }

   }

   public void startRunnableTask() {
      (new BukkitRunnable() {
         public void run() {
            if (RandomTP.this.counter <= 0) {
               RandomTP.this.counter = 180;
               RandomTP.this.backup();
            }

            if (RandomTP.this.getPlayersInTeleportProcess().size() > 0) {
               ArrayList<UUID> array = new ArrayList<>(RandomTP.this.getPlayersInTeleportProcess().keySet());
               for(UUID uuid : array) {
                  Player p = RandomTP.this.getServer().getPlayer(uuid);
                  if (p == null) {
                     RandomTP.this.getPlayersInTeleportProcess().remove(uuid);
                  } else if (!((TeleportTask)RandomTP.this.getPlayersInTeleportProcess().get(uuid)).updateTeleport()) {
                     RandomTP.this.getPlayersInTeleportProcess().remove(uuid);
                  }
               }
            }

            --RandomTP.this.counter;
         }
      }).runTaskTimer(this, 20L, 20L);
   }

   public Map<UUID,TeleportTask> getPlayersInTeleportProcess() {
      return this.playersInTeleportProcess;
   }

   public Map<UUID,Location> getLocationsToTeleport() {
      return this.locationsToTeleport;
   }

   public Map<Integer,Portal> getPortals() {
      return this.portals;
   }

   public PortalUtil getPortalUtils() {
      return this.portalUtils;
   }

   public String getPrefix() {
      return this.prefix;
   }

   public Economy getEconomy() {
      return this.economy;
   }

   public Lang getLang() {
      return this.langFile;
   }

   public Database getDb() {
      return this.databaseFile;
   }

   public SignUtil getSignUtil() {
      return this.signUtils;
   }

   public ExternalReferences getExternalReferences() {
      return this.externalRefs;
   }

   public ReadableHelp getHelp() {
      return this.help;
   }

   public GUI getGui() {
      return this.gui;
   }

   public boolean isSpawnBoats() {
      return this.spawnBoats;
   }

   public Material getMaterial(int ID, int Data) {
      RMaterial mat = new RMaterial();
      return this.version == VersionUtil.ServerVersion.ONE_SEVEN_TO_ONE_TWELVE ? mat.getMaterial(ID, Data) : mat.getUpperMaterial(ID, Data);
   }
}
