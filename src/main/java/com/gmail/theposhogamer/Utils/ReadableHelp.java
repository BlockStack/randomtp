package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.List;

import com.gmail.theposhogamer.RandomTP;

import org.bukkit.command.CommandSender;

public class ReadableHelp {
   private RandomTP plugin;
   List<String> signs = new ArrayList<>();
   //HashMap signcmd = new HashMap<>();

   public ReadableHelp(RandomTP plugin) {
      this.plugin = plugin;
   }

   public void reload() {
      this.signs.clear();

      for(int i = 0; i < this.plugin.getSignUtil().getSigns().size(); ++i) {
         RSign sign = (RSign)this.plugin.getSignUtil().getSigns().get(i);
         String fstr = "§c#§f" + (i + 1) + " §cLocation(x,y,z,world): §f" + sign.x + " " + sign.y + " " + sign.z + " " + sign.world;
         this.signs.add(fstr);
      }

   }

   public void sendMessage(CommandSender p, int page) {
      int hojas = this.getPages();
      if (page > hojas || page < 1) {
         page = 1;
      }

      if (hojas == 0) {
         page = 0;
      }

      p.sendMessage(this.plugin.getPrefix() + "§cSign List (" + page + "/" + hojas + "):");
      for(String sign : this.getPageSigns(p, page, hojas)) {
         p.sendMessage(sign);
      }

   }

   public int getPages() {
      double dsize = Double.valueOf((double)this.signs.size()) / 5.0D;
      if (dsize % 1.0D != 0.0D) {
         ++dsize;
      }

      int size = (int)dsize;
      return size;
   }

   public List<String> getPageSigns(CommandSender p, int page, int pages) {
      List<String> list = new ArrayList<>();
      if (pages > 0) {
         int totales = pages;
         if (pages % 1 != 0) {
            totales = pages + 1;
         }

         //int checker = 0;

         for(int i = 0; i < totales + 1; ++i) {
            if (page == i) {
               for(int i2 = page * 5 - 5; i2 < page * 5; ++i2) {
                  if (i2 <= this.signs.size() - 1) {
                     String sign = (String)this.signs.get(i2);
                     if (sign != null) {
                        list.add(sign);
                     }
                  }
               }
            } else {
               //++checker;
            }
         }
      }

      return list;
   }
}
