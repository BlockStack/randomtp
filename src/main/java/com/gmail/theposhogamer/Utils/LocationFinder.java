package com.gmail.theposhogamer.Utils;

import com.gmail.theposhogamer.RandomTP;
import java.util.Random;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.scheduler.BukkitRunnable;

public class LocationFinder {

   private static final Random random = new Random(System.nanoTime());

   public LocationFinder(final RandomTP plugin, final String world, final int dist, final UUID uuid) {
      final boolean is1_8 = plugin.getServer().getVersion().contains("1.8");
      final ExternalReferences extRef = plugin.getExternalReferences();
      (new BukkitRunnable() {
         int tries = 0;

         public void run() {
            boolean valid = true;
            if (this.tries <= 10000) {
               World w = plugin.getServer().getWorld(world);
               if (w != null) {
                  Environment actual = w.getEnvironment();
                  boolean isEnd = actual == Environment.THE_END;
                  boolean isNether = actual == Environment.NETHER;
                  int distance = dist;
                  int min = distance - (distance / 2);
                  if (isEnd) {
                     min = 1;
                     if (is1_8) {
                        distance = 100;
                     }
                  }

                  int x = LocationFinder.this.randomSymbol(LocationFinder.this.getRandom(min, distance));
                  int z = LocationFinder.this.randomSymbol(LocationFinder.this.getRandom(min, distance));
                  //force chunk load before safety check
                  new Location(w,x,128,z).getChunk().load(true);
                  Block highestBlock = w.getHighestBlockAt(x, z).getRelative(BlockFace.DOWN);
                  int y = highestBlock.getY() + 1;
                  Location probLoc = new Location(w, (double)x, (double)y, (double)z);
                  if (isNether) {
                     int safey = LocationFinder.this.getSafeY(probLoc);
                     if (safey != -1) {
                        probLoc.setY((double)safey);
                     } else {
                        valid = false;
                     }
                  } else if (isEnd) {
                     if (!LocationFinder.this.isSafe(probLoc.clone().add(0.0D, 1.0D, 0.0D))) {
                        valid = false;
                     }

                     if (highestBlock.isEmpty()) {
                        valid = false;
                     }
                  }

                  if (extRef.residence && !extRef.isValidResidence(probLoc)) {
                     valid = false;
                  }

                  if (extRef.worldBorder && !extRef.isValidWorldBorder(probLoc)) {
                     valid = false;
                  }

                  if (extRef.factions && !extRef.isValidFactions(probLoc)) {
                     valid = false;
                  }

                  if (extRef.griefprevention && !extRef.isValidGriefPrevention(probLoc)) {
                     valid = false;
                  }

                  if (extRef.towny && !extRef.isValidTowny(probLoc)) {
                     valid = false;
                  }

                  if (extRef.worldguard && !extRef.isValidWG(probLoc)) {
                     valid = false;
                  }

                  if (!extRef.isValid(probLoc)) {
                     valid = false;
                  }

                  if (!LocationFinder.isSafeLocation(probLoc)) {
                     valid = false;
                  }

                  if (LocationFinder.this.isWater(probLoc)) {
                     valid = false;
                  }

                  if (valid) {
                     this.cancel();
                     plugin.getLocationsToTeleport().put(uuid, probLoc);
                     return;
                  }

                  ++this.tries;
               } else {
                  plugin.log("§cThe world " + world + " does not exist or is invalid");
                  this.cancel();
               }
            } else {
               this.cancel();
               plugin.getLocationsToTeleport().put(uuid, null);
            }

         }
      }).runTaskTimer(plugin, 0L, 0L);
   }

   public static boolean isSafeLocation(Location location) {
      Block feet = location.getBlock();
      if (!feet.getType().isTransparent() && !feet.getLocation().add(0.0D, 1.0D, 0.0D).getBlock().getType().isTransparent()) {
         return false;
      } else {
         Block head = feet.getRelative(BlockFace.UP);
         if (!head.getType().isTransparent()) {
            return false;
         } else {
            Block ground = feet.getRelative(BlockFace.DOWN);
            return ground.getType().isSolid();
         }
      }
   }

   private int getSafeY(Location location) {
      int x = location.getBlockX();
      int z = location.getBlockZ();
      int air = 0;

      for(int y = 20; y < 126; ++y) {
         if (location.getWorld().getBlockAt(x, y, z).getType() == Material.AIR) {
            if (y - air == 1 && location.getWorld().getBlockAt(x, air - 1, z).getType() == Material.NETHERRACK) {
               return air;
            }

            air = y;
         }
      }

      return -1;
   }

   public boolean isSafe(Location location) {
      int radius = 2;
      int count = 0;

      for(int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; ++x) {
         for(int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; ++z) {
            if (location.getWorld().getBlockAt(x, location.getBlockY(), z).getType() != Material.AIR) {
               ++count;
            }
         }
      }

      if (count <= 1) {
         return true;
      } else {
         return false;
      }
   }

   public int getRandom(int lower, int upper) {
      return random.nextInt(upper - lower + 1) + lower;
   }

   private Integer randomSymbol(int number) {
      int probability = random.nextInt(100);
      return probability >= 50 ? number * -1 : number;
   }

   public boolean isBiome(Location loc, Biome b) {
      return loc.getWorld().getBiome(loc.getBlockX(), loc.getBlockZ()) == b;
   }

   public boolean isWater(Location loc) {
      return this.isBiome(loc, Biome.OCEAN) || this.isBiome(loc, Biome.DEEP_OCEAN);
   }
}
