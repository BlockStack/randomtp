package com.gmail.theposhogamer.Utils.Enums;

public enum BiomeType {
   NORMAL,
   NETHER,
   END;
}
