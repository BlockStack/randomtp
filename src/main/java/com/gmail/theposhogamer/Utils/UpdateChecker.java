package com.gmail.theposhogamer.Utils;

import com.gmail.theposhogamer.RandomTP;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class UpdateChecker {
   private URL checkURL;
   private String newVersion;
   private RandomTP plugin;

   public UpdateChecker(RandomTP plugin, int projectID) {
      this.plugin = plugin;
      this.newVersion = plugin.getDescription().getVersion();
      plugin.log("§cChecking for updates...");

      try {
         this.checkURL = new URL("https://api.spigotmc.org/legacy/update.php?resource=" + projectID);

         try {
            this.broadCastResult();
         } catch (Exception var4) {
            plugin.log("§cCouldn't check for updates, don't you have internet connection?");
         }
      } catch (MalformedURLException var5) {
         plugin.log("§cCouldn't check for updates, don't you have internet connection?");
      }

   }

   public String getNewVersion() throws Exception {
      URLConnection con = this.checkURL.openConnection();
      this.newVersion = (new BufferedReader(new InputStreamReader(con.getInputStream()))).readLine();
      return this.newVersion;
   }

   public int versionCompare(String paramString1, String paramString2) {
      String[] arrayOfString1 = paramString1.split("_")[0].split("\\.");
      String[] arrayOfString2 = paramString2.split("_")[0].split("\\.");

      int i;
      for(i = 0; i < arrayOfString1.length && i < arrayOfString2.length && arrayOfString1[i].equals(arrayOfString2[i]); ++i) {
      }

      if (i < arrayOfString1.length && i < arrayOfString2.length) {
         int j = Integer.valueOf(arrayOfString1[i]).compareTo(Integer.valueOf(arrayOfString2[i]));
         return Integer.signum(j);
      } else {
         return Integer.signum(arrayOfString1.length - arrayOfString2.length);
      }
   }

   public void broadCastResult() throws Exception {
      String newvers = this.getNewVersion();

      try {
         switch(this.versionCompare(this.plugin.getDescription().getVersion(), newvers)) {
         case -1:
            this.plugin.log("§cYour version is outdated, please download our new update version §a" + newvers);
            this.plugin.state = "§c§lOutdated";
            break;
         case 0:
            this.plugin.log("§aYou are using the last stable version of the plugin §6" + newvers);
            this.plugin.state = "§a§lStable";
            break;
         case 1:
            this.plugin.log("§bYou are using one of the beta snapshots, you might be my developer, published version §6" + newvers + " §b& yours is §6" + this.plugin.getDescription().getVersion());
            this.plugin.state = "§b§lSnapshot";
            break;
         default:
            this.plugin.log("§cCouldn't check for updates, don't you have internet connection?");
         }
      } catch (Exception var3) {
         this.plugin.log("§cCouldn't check for updates, don't you have internet connection?");
      }

   }
}
