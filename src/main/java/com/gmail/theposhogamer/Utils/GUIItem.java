package com.gmail.theposhogamer.Utils;

import java.util.List;

public class GUIItem {
   public String id;
   public int slot;
   public String name;
   public int cost;
   public int distance;
   public String world;
   public int cooldown;
   public String permission;
   public List<String> lore;

   public GUIItem(String id, int slot, String name, int cost, int distance, String world, int cooldown, String permission, List<String> lore) {
      this.id = id;
      this.slot = slot;
      this.name = name;
      this.cost = cost;
      this.distance = distance;
      this.world = world;
      this.cooldown = cooldown;
      this.permission = permission;
      this.lore = lore;
   }
}
