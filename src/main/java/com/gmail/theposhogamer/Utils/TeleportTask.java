package com.gmail.theposhogamer.Utils;

import com.gmail.theposhogamer.RandomTP;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class TeleportTask {
   private Player p;
   private Location loc;
   private int i = 20;
   private boolean ready = false;
   private RandomTP plugin;
   private PotionEffect eff;
   private GameMode gamemode;
   private boolean flying;
   private boolean allow_flight;

   public TeleportTask(Player p, RandomTP plugin, String world, int distance) {
      this.p = p;
      this.plugin = plugin;
      this.gamemode = this.p.getGameMode();
      this.flying = this.p.isFlying();
      this.allow_flight = this.p.getAllowFlight();
      new LocationFinder(plugin, world, distance, p.getUniqueId());
   }

   public boolean updateTeleport() {
      if (this.p != null && this.p.isOnline()) {
         if (!this.ready) {
            UUID uuid = this.p.getUniqueId();
            if (this.plugin.getLocationsToTeleport().containsKey(uuid)) {
               if (this.plugin.getLocationsToTeleport().get(uuid) == null) {
                  this.p.sendMessage("§cAn exception occurred, please contact the admin to expand the teleporting area");
                  return false;
               }

               this.loc = ((Location)this.plugin.getLocationsToTeleport().get(uuid)).clone();
               this.plugin.getLocationsToTeleport().remove(uuid);
               this.ready = true;
               Iterator<PotionEffect> potionEffectIterator = this.p.getActivePotionEffects().iterator();

               while(potionEffectIterator.hasNext()) {
                  PotionEffect eff = (PotionEffect)potionEffectIterator.next();
                  if (eff.getType() == PotionEffectType.DAMAGE_RESISTANCE) {
                     this.eff = new PotionEffect(eff.getType(), eff.getDuration(), eff.getAmplifier());
                  }
               }
            } else {
               this.p.sendMessage(this.plugin.getLang().getConfig().getString("SEARCHINGLOCATION").replace("&", "§"));
            }

            return true;
         } else {
            --this.i;
            if (this.i > 18 && this.i <= 20) {
               this.p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
               this.p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 70, 100));
               if (!this.p.getLocation().equals(this.loc)) {
                  if (this.i == 19) {
                     this.p.setAllowFlight(true);
                     this.p.setFlying(true);
                     Block block = this.loc.getWorld().getBlockAt(this.loc);
                     block.setType(Material.AIR);
                     block.getRelative(BlockFace.UP).getState().update();
                  }

                  this.loc.add(0.0D, 1.0D, 0.0D);
                  this.p.teleport(this.loc, TeleportCause.PLUGIN);
                  this.p.setFallDistance(-150.0F);
               }
            } else if (this.i == 18) {
               this.p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
               this.p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 70, 100));
               this.p.teleport(this.p.getLocation().add(0.0D, 1.0D, 0.0D).add(0.0D, 0.5D, 0.0D), TeleportCause.PLUGIN);
               this.p.setFlying(false);
               this.p.setAllowFlight(false);
               this.p.sendMessage(this.plugin.getLang().getConfig().getString("SUCCESSTELEPORT").replace("&", "§"));
               Location ploc = this.p.getLocation();
               List<Block> list = this.plugin.getSignUtil().getNearbyBlocks(ploc, 3);
               if (this.plugin.isSpawnBoats()) {
                  for(int i = 0; i < list.size(); ++i) {
                     Block b = (Block)list.get(i);
                     Material mat = b.getType();
                     if (mat == this.plugin.getMaterial(8, 0) || mat == this.plugin.getMaterial(9, 0)) {
                        Entity boat = ploc.getWorld().spawnEntity(ploc, EntityType.BOAT);
                        boat.setPassenger(this.p);
                        break;
                     }
                  }
               }
            } else {
               if (this.i < 18 && this.i > 1) {
                  this.p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                  this.p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 70, 100));
                  this.p.setFireTicks(0);
                  if (this.i == 17) {
                     this.p.setAllowFlight(this.allow_flight);
                     this.p.setFlying(this.flying);
                     this.p.setGameMode(this.gamemode);
                  }

                  return true;
               }

               if (this.i == 0) {
                  this.p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                  if (this.eff != null) {
                     this.p.addPotionEffect(this.eff);
                  }

                  return false;
               }
            }

            return true;
         }
      } else {
         return false;
      }
   }
}
