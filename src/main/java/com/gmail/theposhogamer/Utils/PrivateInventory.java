package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PrivateInventory {
   private Inventory inv;
   private static Map<UUID, PrivateInventory> inventories = new HashMap<>();
   private Map<Integer,ClickRunnable> runnables;
   private UUID uuid;

   public PrivateInventory(String name, int size, UUID uuid) {
      this(name, size, uuid, (ItemStack)null);
   }

   public PrivateInventory(String name, int size, UUID uuid, ItemStack placeholder) {
      this.runnables = new HashMap<>();
      this.uuid = uuid;
      if (size != 0) {
         this.inv = Bukkit.createInventory((InventoryHolder)null, size, name);
         if (placeholder != null) {
            for(int i = 0; i < size; ++i) {
               this.inv.setItem(i, placeholder);
            }
         }

         this.register();
      }
   }

   public Inventory getInventory() {
      return this.inv;
   }

   public int getSize() {
      return this.inv.getSize();
   }

   public void setItem(ItemStack itemstack, Integer slot, PrivateInventory.ClickRunnable executeOnClick) {
      this.setItem(itemstack, (String)null, slot, executeOnClick);
   }

   public void setItem(ItemStack itemstack, String displayname, Integer slot, PrivateInventory.ClickRunnable executeOnClick, String... description) {
      ItemMeta im = itemstack.getItemMeta();
      if (displayname != null) {
         im.setDisplayName(ChatColor.RED + displayname);
      }

      if (description != null) {
         List<String> lore = new ArrayList<>(Arrays.asList(description));
         im.setLore(lore);
      }

      itemstack.setItemMeta(im);
      this.inv.setItem(slot, itemstack);
      this.runnables.put(slot, executeOnClick);
   }

   public void removeItem(int slot) {
      this.inv.setItem(slot, new ItemStack(Material.AIR));
   }

   public void setItem(ItemStack itemstack, Integer slot) {
      this.inv.setItem(slot, itemstack);
   }

   public static Listener getListener() {
      return new Listener() {
         @EventHandler
         public void onClick(InventoryClickEvent e) {
            HumanEntity clicker = e.getWhoClicked();
            if (clicker instanceof Player) {
               if (e.getCurrentItem() == null) {
                  return;
               }

               Player p = (Player)clicker;
               if (p != null) {
                  UUID uuid = p.getUniqueId();
                  if (PrivateInventory.inventories.containsKey(uuid)) {
                     PrivateInventory current = (PrivateInventory)PrivateInventory.inventories.get(uuid);
                     if (!p.getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase(current.getInventory().getTitle())) {
                        return;
                     }

                     int slot = e.getSlot();
                     if (current.runnables.get(slot) != null) {
                        ((PrivateInventory.ClickRunnable)current.runnables.get(slot)).run(e);
                     }

                     e.setCancelled(true);
                  }
               }
            }

         }

         @EventHandler
         public void onClose(InventoryCloseEvent e) {
            if (e.getPlayer() instanceof Player) {
               if (e.getInventory() == null) {
                  return;
               }

               Player p = (Player)e.getPlayer();
               UUID uuid = p.getUniqueId();
               if (PrivateInventory.inventories.containsKey(uuid)) {
                  ((PrivateInventory)PrivateInventory.inventories.get(uuid)).unRegister();
               }
            }

         }
      };
   }

   public void openInventory(Player player) {
      Inventory inv = this.getInventory();
      InventoryView openInv = player.getOpenInventory();
      if (openInv != null) {
         Inventory openTop = player.getOpenInventory().getTopInventory();
         if (openTop != null && openTop.getTitle().equalsIgnoreCase(inv.getTitle())) {
            openTop.setContents(inv.getContents());
         } else {
            player.openInventory(inv);
         }

         this.register();
      }

   }

   private void register() {
      inventories.put(this.uuid, this);
   }

   private void unRegister() {
      inventories.remove(this.uuid);
   }

   @FunctionalInterface
   public interface ClickRunnable {
      void run(InventoryClickEvent var1);
   }

   @FunctionalInterface
   public interface CloseRunnable {
      void run(InventoryCloseEvent var1);
   }
}
