package com.gmail.theposhogamer.Utils;

public class RSign {
   public final int x;
   public final int y;
   public final int z;
   public final String world;
   public final int blocks;
   public final int price;
   public int cooldown;

   public RSign(int x, int y, int z, String world, int blocks, int price, int cooldown) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.world = world;
      this.blocks = blocks;
      this.price = price;
      this.cooldown = cooldown;
   }
}
