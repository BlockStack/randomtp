package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import com.gmail.theposhogamer.RandomTP;
import com.gmail.theposhogamer.Portals.Portal;
import com.gmail.theposhogamer.Portals.RegionBorderCheck;

import org.apache.commons.lang.math.IntRange;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class PortalUtil {
   private RandomTP plugin;
   private Map<UUID,Portal> portalCreator = new HashMap<>();

   public PortalUtil(RandomTP main) {
      this.plugin = main;
   }

   public Map<UUID,Portal> getPortalCreator() {
      return this.portalCreator;
   }

   public void startPortalCheckTask() {
      (new BukkitRunnable() {
         public void run() {
            PortalUtil.this.checkAllAndTeleport();
         }
      }).runTaskTimer(this.plugin, 10L, 10L);
   }

   public void checkAllAndTeleport() {
      for(Player p : this.plugin.getServer().getOnlinePlayers()) {
         for(Portal portal : this.plugin.getPortals().values()) {
            this.checkAndTeleport(p, portal);
         }
      }
   }

   public void checkAndTeleport(Player p, Portal portal) {
      Location loc = p.getLocation();
      if (this.playerIsNearToPortal(loc, portal) && this.isInPortal(loc, portal) && !this.plugin.getPlayersInTeleportProcess().containsKey(p.getUniqueId())) {
         this.usePortal(p, portal);
      }
   }

   public boolean playerIsNearToPortal(Location loc, Portal portal) {
      return loc.getWorld().getName().equalsIgnoreCase(portal.down.getWorld().getName()) && loc.distance(portal.middle) < portal.height;
   }

   public boolean isWaterInPortal(Block b) {
      for(Portal portal : this.plugin.getPortals().values()) {
         if (this.isInPortal(b.getLocation(), portal)) {
            return true;
         }
      }
      return false;
   }

   public Portal getPortalByName(String s) {
      for(Portal portal : this.plugin.getPortals().values()) {
         if (portal.portalName.equalsIgnoreCase(s)) {
            return portal;
         }
      }
      return null;
   }

   public Portal getPortalByNearBlocks(Location loc) {
      for(Block block : this.plugin.getSignUtil().getNearbyBlocks(loc, 2)) {
         for(Portal portal : this.plugin.getPortals().values()) {
            if (this.isInPortal(this.getCenter(block.getLocation()), portal)) {
               return portal;
            }
         }
      }
      return null;
   }

   public void set(Location loc1, Location loc2, Material type) {
      int topBlockX = loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX();
      int bottomBlockX = loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX();
      int topBlockY = loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY();
      int bottomBlockY = loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY();
      int topBlockZ = loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ();
      int bottomBlockZ = loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ();

      for(int x = bottomBlockX; x <= topBlockX; ++x) {
         for(int z = bottomBlockZ; z <= topBlockZ; ++z) {
            for(int y = bottomBlockY; y <= topBlockY; ++y) {
               Block block = loc1.getWorld().getBlockAt(x, y, z);
               block.setType(type);
               block.getState().update();
            }
         }
      }

   }

   public List<Block> getBlocks(Location loc1, Location loc2) {
      List<Block> list = new ArrayList<>();
      int topBlockX = loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX();
      int bottomBlockX = loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX();
      int topBlockY = loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY();
      int bottomBlockY = loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY();
      int topBlockZ = loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ();
      int bottomBlockZ = loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ();

      for(int x = bottomBlockX; x <= topBlockX; ++x) {
         for(int z = bottomBlockZ; z <= topBlockZ; ++z) {
            for(int y = bottomBlockY; y <= topBlockY; ++y) {
               Block block = loc1.getWorld().getBlockAt(x, y, z);
               list.add(block);
            }
         }
      }

      return list;
   }

   public Location getCenter(Location loc) {
      return new Location(loc.getWorld(), this.getRelativeCoord(loc.getBlockX()), this.getRelativeCoord(loc.getBlockY()), this.getRelativeCoord(loc.getBlockZ()));
   }

   private double getRelativeCoord(int i) {
      double d = (double)i;
      d = d < 0.0D ? d - 0.5D : d + 0.5D;
      return d;
   }

   public List<Block> getNearbyBlocks(Location location, int radius) {
      List<Block> blocks = new ArrayList<>();

      for(int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; ++x) {
         for(int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; ++y) {
            for(int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; ++z) {
               blocks.add(location.getWorld().getBlockAt(x, y, z));
            }
         }
      }

      return blocks;
   }

   public boolean inCuboid(Location origin, Location l1, Location l2) {
      return (new IntRange(l1.getX(), l2.getX())).containsDouble(origin.getX()) && (new IntRange(l1.getY(), l2.getY())).containsDouble(origin.getY()) && (new IntRange(l1.getZ(), l2.getZ())).containsDouble(origin.getZ());
   }

   public boolean isInPortal(Location loc, Portal portal) {
      Location l1 = portal.up.clone();
      Location l2 = portal.down.clone();
      RegionBorderCheck border = new RegionBorderCheck(l1.toVector(), l2.toVector());
      return border.contains(loc);
   }

   public String portalToString(Portal portal) {
      String name = portal.portalName.replace(" ", "{b}");
      int id = portal.id;
      String up = this.locToString(portal.up);
      String down = this.locToString(portal.down);
      String w = portal.world.replace(" ", "{b}");
      int blocks = portal.blocks;
      int price = portal.price;
      int cool = portal.cooldown;
      String perm = portal.permission.replace(" ", "{b}");
      return name + "=" + id + "=" + up + "=" + down + "=" + w + "=" + blocks + "=" + price + "=" + cool + "=" + perm;
   }

   public Portal stringToPortal(String s) {
      String[] comp = s.split("=");
      String name = comp[0].replace("{b}", " ");
      int id = Integer.valueOf(comp[1]);
      Location up = this.stringToLoc(comp[2]);
      Location down = this.stringToLoc(comp[3]);
      String w = comp[4].replace("{b}", " ");
      int b = Integer.valueOf(comp[5]);
      int price = Integer.valueOf(comp[6]);
      int cool = Integer.valueOf(comp[7]);
      String perm = comp[8].replace("{b}", " ");
      return new Portal(name, id, up, down, w, b, price, cool, perm);
   }

   public void savePortals() {
      List<String> list = new ArrayList<String>();
      if (this.plugin.getDb().getConfig().getStringList("Portals") != null) {
         list = this.plugin.getDb().getConfig().getStringList("Portals");
      }

      for(Portal p : this.plugin.getPortals().values()) {
         String pStr = this.portalToString(p);
         if (!list.contains(pStr)) {
            list.add(pStr);
         }
      }

      this.plugin.getDb().getConfig().set("Portals", list);
      this.plugin.getDb().save();
      this.plugin.getDb().reload();
   }

   public void loadPortals() {
      List<String> list = new ArrayList<>();
      if (this.plugin.getDb().getConfig().getStringList("Portals") != null) {
         list = this.plugin.getDb().getConfig().getStringList("Portals");
      }

      for(int i = 0; i < list.size(); ++i) {
         String p = list.get(i);
         Portal portal = this.stringToPortal(p);
         this.plugin.getPortals().put(portal.id, portal);
      }

      this.plugin.log("§7" + list.size() + " portals were loaded");
   }

   public String locToString(Location loc) {
      int x = loc.getBlockX();
      int y = loc.getBlockY();
      int z = loc.getBlockZ();
      String w = loc.getWorld().getName();
      return x + "#" + y + "#" + z + "#" + w;
   }

   public Location stringToLoc(String s) {
      String[] comp = s.split("#");
      int x = Integer.valueOf(comp[0]);
      int y = Integer.valueOf(comp[1]);
      int z = Integer.valueOf(comp[2]);
      String w = comp[3];
      return new Location(this.plugin.getServer().getWorld(w), (double)x, (double)y, (double)z);
   }

   public int getRandom(int lower, int upper) {
      return (new Random()).nextInt(upper - lower + 1) + lower;
   }

   public void usePortal(Player p, Portal portal) {
      if (this.plugin.getSignUtil().canUseTeleport(p)) {
         if (!portal.permission.equalsIgnoreCase("noperms") && !p.hasPermission(portal.permission)) {
            p.sendMessage(this.plugin.getLang().getConfig().getString("PORTAL.NOPERMISSION").replace("&", "§"));
         } else {
            UUID uuid = p.getUniqueId();
            if (this.plugin.getSignUtil().cooldownportal.containsKey(uuid)) {
               List<String> cooldowns = this.plugin.getSignUtil().cooldownportal.get(uuid);

               for(int i = 0; i < cooldowns.size(); ++i) {
                  String string = (String)cooldowns.get(i);
                  String[] rportal = string.split("_");
                  Long time_elapsed = System.currentTimeMillis() - Long.valueOf(rportal[0]);
                  Integer otherportal = Integer.valueOf(rportal[1]);
                  if (portal.id == otherportal) {
                     double seconds = (double)time_elapsed / 1000.0D;
                     if (seconds < (double)portal.cooldown) {
                        p.sendMessage(this.plugin.getLang().getConfig().getString("COOLDOWNLEFT").replace("&", "§").replace("%variable%", TimeUtils.calculateTime((long)((double)portal.cooldown - seconds))));
                        return;
                     }

                     this.plugin.getSignUtil().cooldownportal.get(uuid).remove(string);
                  }
               }
            }

            if (this.plugin.getExternalReferences().vault) {
               try {
                  double money = this.plugin.getEconomy().getBalance(p);
                  int cost = portal.price;
                  if (cost != 0) {
                     if (money < (double)cost) {
                        p.sendMessage(this.plugin.getLang().getConfig().getString("INSSUFICIENTMONEY").replace("&", "§").replace("%variable%", String.valueOf(cost)));
                        return;
                     }

                     p.sendMessage(this.plugin.getLang().getConfig().getString("SUCCESSFULBUY").replace("&", "§").replace("%variable%", String.valueOf(cost)));
                     this.plugin.getEconomy().withdrawPlayer(p, (double)cost);
                  }
               } catch (Exception var12) {
                  this.plugin.log("§cYou are using Vault but you don't have an economy plugin");
               }
            }

            this.plugin.getPlayersInTeleportProcess().put(uuid, new TeleportTask(p, this.plugin, portal.world, portal.blocks));
            if (portal.cooldown != 0) {
               List<String> cooldowns = new ArrayList<>();
               if (this.plugin.getSignUtil().cooldownportal.containsKey(uuid)) {
                  cooldowns = this.plugin.getSignUtil().cooldownportal.get(uuid);
               }

               cooldowns.add(System.currentTimeMillis() + "_" + portal.id);
               this.plugin.getSignUtil().cooldownportal.put(uuid, cooldowns);
            }

         }
      }
   }

   public void openPortalCreator(final Player p) {
      final UUID uuid = p.getUniqueId();
      int newid = -1;
      if (!this.portalCreator.containsKey(uuid)) {
         for(int i = 0; i < Integer.MAX_VALUE; ++i) {
            newid = this.getRandom(0, 2147483646);
            if (!this.plugin.getPortals().containsKey(newid)) {
               break;
            }
         }

         this.portalCreator.put(uuid, new Portal("Unnamed", newid, (Location)null, (Location)null, (String)null, 0, 0, 0, "noperms"));
         p.sendMessage(this.plugin.getPrefix() + "A new portal setup has been created for you. " + "New portal ID: " + newid);
      } else {
         newid = ((Portal)this.portalCreator.get(uuid)).id;
         p.sendMessage(this.plugin.getPrefix() + "§aLast portal setup has been loaded for you. " + "New portal ID: " + newid);
      }

      PrivateInventory inv = new PrivateInventory("§dSetupID: " + newid, 27, uuid, (ItemStack)null);
      inv.setItem(new ItemStack(Material.LAVA_BUCKET), "§b§lPortal's Eraser", 0, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack tag = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta tag_meta = tag.getItemMeta();
            tag_meta.setDisplayName("§c§lRTP §e- §6ϟ §cPortal Eraser Tool §6ϟ");
            tag.setItemMeta(tag_meta);
            p.getInventory().addItem(new ItemStack[]{tag});
            p.updateInventory();
            p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A portal eraser tool has been added to you inventory, now" + " you only need to put the lava near the portal to erase it");
         }
      }, "§7Gives you an eraser tool", "§7with that tool §b§lRight Click", "§7to put the lava near a", "§7portal to completely erase it", "§a§l§nClick to get the tool");
      inv.setItem(new ItemStack(Material.REDSTONE_BLOCK), "§c§aClick to erase the portal", 18, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cYou portal setup has been erased. " + "Erased portal ID: " + ((Portal)PortalUtil.this.portalCreator.get(uuid)).id);
            PortalUtil.this.portalCreator.remove(uuid);
            p.closeInventory();
         }
      }, "§7Removes all draft saved", "§7portal information");
      inv.setItem(new ItemStack(Material.NAME_TAG), "§b§lPortal's Name", 10, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your portal name>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your portal name and then open the menu again and right click to submit the name");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your portal name>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     ((Portal)PortalUtil.this.portalCreator.get(uuid)).portalName = dname;
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + dname + " §ais the new name of your portal");
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§lcame on man!" + " §cI only asked you for a renamed nametag with your desired portal name");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the portal name you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(Material.DIAMOND_PICKAXE), "§b§lPortal's Locations", 11, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack tag = new ItemStack(Material.DIAMOND_PICKAXE);
            ItemMeta tag_meta = tag.getItemMeta();
            tag_meta.setDisplayName("§c§lRTP §e- §6ϟ §cPortal Selection Tool §6ϟ");
            tag.setItemMeta(tag_meta);
            p.getInventory().addItem(new ItemStack[]{tag});
            p.updateInventory();
            p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A portal selection tool has been added to you inventory, now" + " you only need to select the portal corners by right/left clicking them with the tool");
         }
      }, "§7Gives you a selection tool", "§7with that tool §b§lRight Click", "§7one of the portal corners", "§7and §b§lLeft Click§7 to select", "§7the other corner with the tool", "§a§l§nClick to get the tool");
      inv.setItem(new ItemStack(this.plugin.getMaterial(66, 0)), "§b§lDistance To Teleport", 12, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your distance>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your distance and then open the menu again and right click to submit the distance");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your distance>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     if (PortalUtil.this.plugin.getSignUtil().isNumeric(dname)) {
                        ((Portal)PortalUtil.this.portalCreator.get(uuid)).blocks = Integer.valueOf(dname);
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + dname + " §ais the new teleport distance in blocks");
                     } else {
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a block distance renamed nametag, §6§loh my gosh!" + " §cI only asked you for a renamed nametag with numbers on it, not text");
                     }
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§lcame on man!" + " §cI only asked you for a renamed nametag with numbers on it");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the block distance you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(Material.DIAMOND), "§b§lTeleport cost", 13, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your teleport cost>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your teleport cost and then open the menu again and right click to submit the cost");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your teleport cost>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     if (PortalUtil.this.plugin.getSignUtil().isNumeric(dname)) {
                        ((Portal)PortalUtil.this.portalCreator.get(uuid)).price = Integer.valueOf(dname);
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + dname + " §ais the new teleport cost");
                     } else {
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a teleport cost renamed nametag, §6§lyis!" + " §cI only asked you for a renamed nametag with a price on it, not strange symbols");
                     }
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§lcame on man!" + " §cI only asked you for a renamed nametag with numbers on it");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the teleport cost you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(Material.GRASS), "§b§lTeleport To World Name", 14, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your world name>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your world name and then open the menu again and right click to submit the world name");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your world name>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     ((Portal)PortalUtil.this.portalCreator.get(uuid)).world = dname;
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + dname + " §ais the teleport to world name of your portal");
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§lcame on man!" + " §cI only asked you for a renamed nametag with your desired teleport to world name");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the world name you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(this.plugin.getMaterial(347, 0)), "§b§lCooldown Time In Seconds", 15, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your cooldown time>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your cooldown time and then open the menu again and right click to submit the cooldown time");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your cooldown time>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     if (PortalUtil.this.plugin.getSignUtil().isNumeric(dname)) {
                        ((Portal)PortalUtil.this.portalCreator.get(uuid)).cooldown = Integer.valueOf(dname);
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + TimeUtils.calculateTime((long)Integer.valueOf(dname)) + " §ais the new cooldown time");
                     } else {
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a cooldown time renamed nametag, §6§llal!" + " §cI only asked you for a renamed nametag with a number of seconds on it, not strange symbols");
                     }
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§lcame on man!" + " §cI only asked you for a renamed nametag with numbers on it");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the teleport cost you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(Material.WRITTEN_BOOK), "§b§lPortal Permission", 16, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            ItemStack i;
            if (e.getClick() == ClickType.LEFT) {
               i = new ItemStack(Material.NAME_TAG);
               ItemMeta tag_meta = i.getItemMeta();
               tag_meta.setDisplayName("<Rename it with your permission>");
               i.setItemMeta(tag_meta);
               p.getInventory().addItem(new ItemStack[]{i});
               p.updateInventory();
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§6A nametag has been added to you inventory, rename it" + "with your permission and then open the menu again and right click to submit the permission");
            } else if (e.getClick() == ClickType.RIGHT) {
               i = p.getItemInHand();
               String empty = PortalUtil.this.plugin.getPrefix() + "§cThe nametag display name is empty please rename it with an anvil";
               if (i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName()) {
                  String dname = i.getItemMeta().getDisplayName();
                  if (dname.equalsIgnoreCase("<Rename it with your permission>")) {
                     p.sendMessage(empty);
                  } else if (i.getType() == Material.NAME_TAG) {
                     ((Portal)PortalUtil.this.portalCreator.get(uuid)).permission = dname;
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§7" + dname + " §ais the new permission of your portal");
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThat is not a renamed nametag, §6§l:(!" + " §cI only asked you for a renamed nametag with your desired permission");
                  }
               } else {
                  p.sendMessage(empty);
               }
            }

         }
      }, "§a§lLeft Click§7 to get the tag", "§7Gives you a tag", "§7you need to rename the tag", "§7with the world name you", "§7want and then hold it in", "§7your hand, open the menu again", "§c§lRight Click §7to Submit ");
      inv.setItem(new ItemStack(Material.EMERALD_BLOCK), "§a§lSave & Create the portal", 26, new PrivateInventory.ClickRunnable() {
         public void run(InventoryClickEvent e) {
            Portal portal = (Portal)PortalUtil.this.portalCreator.get(uuid);
            if (!portal.portalName.equalsIgnoreCase("Unnamed") && !portal.portalName.isEmpty()) {
               if (portal.up != null && portal.down != null) {
                  if (portal.up != null && portal.down != null) {
                     if (portal.world != null && !portal.world.isEmpty()) {
                        if (portal.blocks < 0) {
                           p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe distance cannot be negative");
                        } else if (portal.price < 0) {
                           p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe cost cannot be negative");
                        } else if (portal.cooldown < 0) {
                           p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe cooldown cannot be negative");
                        } else {
                           Portal newportal = new Portal(portal.portalName, portal.id, portal.up, portal.down, portal.world, portal.blocks, portal.price, portal.cooldown, portal.permission);
                           PortalUtil.this.plugin.getPortals().put(newportal.id, newportal);
                           p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§aA new portal has been saved to the database. " + "New portal ID: " + newportal.id);
                           PortalUtil.this.portalCreator.remove(uuid);
                           PortalUtil.this.set(newportal.up, newportal.down, Material.WATER);
                           for(Block b : PortalUtil.this.getBlocks(newportal.up, newportal.down)) {
                              BlockFace[] var9;
                              int var8 = (var9 = BlockFace.values()).length;

                              for(int var7 = 0; var7 < var8; ++var7) {
                                 BlockFace blockface = var9[var7];
                                 Block relative = b.getRelative(blockface);
                                 if (relative.getLocation().distance(b.getLocation()) <= 1.1D && relative.getType() == Material.AIR) {
                                    relative.setType(PortalUtil.this.plugin.getMaterial(36, 0));
                                 }
                              }
                           }
                           
                           PortalUtil.this.savePortals();
                           p.closeInventory();
                        }
                     } else {
                        p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe name of the world that the portal leds to cannot be empty");
                     }
                  } else {
                     p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe portal bounds had not been setted correctly");
                  }
               } else {
                  p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe portal bounds had not been setted correctly");
               }
            } else {
               p.sendMessage(PortalUtil.this.plugin.getPrefix() + "§cThe name of the portal cannot be left empty or unnamed");
            }
         }
      }, "§7Creates the portal and saves", "§7wall the setup data to the", "§7datavase", "§7§a§l§nClick to finish");
      inv.openInventory(p);
   }
}
