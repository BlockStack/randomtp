package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.bekvon.bukkit.residence.Residence;
import com.bekvon.bukkit.residence.protection.ClaimedResidence;
import com.bekvon.bukkit.residence.protection.CuboidArea;
import com.gmail.theposhogamer.RandomTP;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;
import com.palmergames.bukkit.towny.object.TownBlock;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import com.wimbli.WorldBorder.BorderData;
import com.wimbli.WorldBorder.Config;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;

public class ExternalReferences {
	private RandomTP plugin;
	public boolean vault = false;
	public boolean factions = false;
	public boolean worldBorder = false;
	public boolean residence = false;
	public boolean griefprevention = false;
	public boolean towny = false;
	public boolean			worldguard		= false;
	public WorldGuardPlugin	wg				= null;

	public ExternalReferences(RandomTP plugin) {
		this.plugin = plugin;
	}

	public boolean isValidGriefPrevention(Location loc) {
		GriefPrevention griefInstance = GriefPrevention.instance;
		List<XYCoord> chunks = new ArrayList<>();
		for(Block b : this.plugin.getPortalUtils().getNearbyBlocks(loc, 3)) {
			Chunk chunk = loc.getWorld().getBlockAt(b.getLocation()).getChunk();
			XYCoord xycoord = new XYCoord(chunk.getX(), chunk.getZ());
			if (!chunks.contains(xycoord)) {
				chunks.add(xycoord);
				Claim claim = griefInstance.dataStore.getClaimAt(b.getLocation(), false, (Claim) null);
				if (claim != null) {
					return false;
				}
			}
		}

		return true;
	}

	public boolean isValidTowny(Location loc) {
		for(Block b : this.plugin.getPortalUtils().getNearbyBlocks(loc, 3)) {
			TownBlock town = TownyUniverse.getTownBlock(b.getLocation());
			if (town != null) {
				return false;
			}
		}

		return true;
	}

	public boolean isValidWG(Location loc) {
		RegionContainer container = com.sk89q.worldguard.WorldGuard.getInstance().getPlatform().getRegionContainer();
		RegionQuery query = container.createQuery();
		ApplicableRegionSet regionsAtLocation = query.getApplicableRegions(BukkitAdapter.adapt(loc));
		return regionsAtLocation.size() <= 0;
	}

	public boolean isValidFactions(Location loc) {
		Faction faction = null;
		faction = BoardColl.get().getFactionAt(PS.valueOf(loc));
		FactionColl fColl = FactionColl.get();
		return faction == fColl.getNone() || faction == fColl.getSafezone() || faction == fColl.getWarzone();
	}

	public boolean isValidWorldBorder(Location loc) {
		BorderData border = Config.Border(loc.getWorld().getName());
		return border != null && border.insideBorder(loc);
	}

	public boolean isValidResidence(Location loc) {
		Collection<ClaimedResidence> res = Residence.getInstance().getResidenceManager().getResidences().values();
		for(ClaimedResidence residence : res ) {
			if (!residence.isSubzone() && loc.getWorld().getName().equals(residence.getWorld())) {
				for(CuboidArea area : residence.getAreaMap().values()) {
					if (this.isInside(loc, area.getHighLoc(), area.getLowLoc())) {
						return false;
					}
				}
			}
		}

		return true;
	}

	public boolean isInside(Location loc, Location l2, Location l1) {
		int x1 = Math.min(l1.getBlockX(), l2.getBlockX());
		int y1 = Math.min(l1.getBlockY(), l2.getBlockY());
		int z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
		int x2 = Math.max(l1.getBlockX(), l2.getBlockX());
		int y2 = Math.max(l1.getBlockY(), l2.getBlockY());
		int z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		return x >= x1 && x <= x2 && y >= y1 && y <= y2 && z >= z1 && z <= z2;
	}

	public boolean isValid(Location loc) {
		List<Block> list = this.plugin.getSignUtil().getNearbyBlocks(loc.clone(), 3);

		for (int i = 0; i < list.size(); ++i) {
			Material type = ((Block) list.get(i)).getType();
			if (type == this.plugin.getMaterial(10, 0) || type == this.plugin.getMaterial(11, 0)) {
				return false;
			}
		}

		return true;
	}

	public WorldGuardPlugin getWorldGuard() {
		Plugin plugin = this.plugin.getServer().getPluginManager().getPlugin("WorldGuard");
		return plugin != null && plugin instanceof WorldGuardPlugin ? (WorldGuardPlugin) plugin : null;
	}
}
