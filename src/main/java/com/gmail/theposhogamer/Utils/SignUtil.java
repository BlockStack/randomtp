package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.gmail.theposhogamer.RandomTP;
import com.gmail.theposhogamer.Portals.Portal;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class SignUtil {
   private RandomTP plugin;
   private ArrayList<RSign> signs = new ArrayList<>();
   public Map<UUID,Integer> uuidCooldown = new HashMap<>();
   public Map<UUID,List<String>> cooldown = new HashMap<>();
   public Map<UUID,List<String>> cooldowngui = new HashMap<>();
   public Map<UUID,List<String>> cooldownportal = new HashMap<>();

   public SignUtil(RandomTP main) {
      this.plugin = main;
   }

   public void loadSigns(boolean startup) {
      this.getSigns().clear();
      List<String> signs = this.plugin.getDb().getConfig().getStringList("Signs");
      if (signs != null) {
         int amount = 0;

         for(int i = 0; i < signs.size(); ++i) {
            String customLoc = (String)signs.get(i);
            this.signs.add(this.stringToRSign(customLoc));
            ++amount;
         }

         if (startup) {
            if (amount > 1) {
               this.plugin.log("§7" + amount + " signs were loaded");
            } else {
               this.plugin.log("§7" + amount + " sign was loaded");
            }
         }
      } else if (startup) {
         this.plugin.log("§7No signs to load were found");
      }

   }

   public void unloadSigns() {
      List<RSign> signs = this.getSigns();
      List<String> toString = new ArrayList<>();

      for(int i = 0; i < signs.size(); ++i) {
         RSign loc = (RSign)signs.get(i);
         toString.add(this.RSignToString(loc));
      }

      this.plugin.getDb().getConfig().set("Signs", toString);
   }

   public List<RSign> getSigns() {
      return this.signs;
   }

   public void saveDatabase() {
      this.plugin.getDb().save();
   }

   public void reloadSigns(boolean startup) {
      this.unloadSigns();
      this.loadSigns(startup);
   }

   public void backupSigns() {
      this.reloadSigns(false);
   }

   public boolean isSign(Location loc) {
      List<RSign> signs = this.signs;
      int x = loc.getBlockX();
      int y = loc.getBlockY();
      int z = loc.getBlockZ();
      String world = loc.getWorld().getName();

      for(int i = 0; i < signs.size(); ++i) {
         RSign cloc = (RSign)signs.get(i);
         if (cloc.x == x && cloc.y == y && cloc.z == z && world.equalsIgnoreCase(cloc.world)) {
            return true;
         }
      }

      return false;
   }

   public RSign getSign(Location loc) {
      List<RSign> signs = this.getSigns();
      int x = loc.getBlockX();
      int y = loc.getBlockY();
      int z = loc.getBlockZ();
      String world = loc.getWorld().getName();

      for(int i = 0; i < signs.size(); ++i) {
         RSign cloc = (RSign)signs.get(i);
         if (cloc.x == x && cloc.y == y && cloc.z == z && world.equalsIgnoreCase(cloc.world)) {
            return cloc;
         }
      }

      return null;
   }

   public boolean removeSign(Location loc) {
      if (this.isSign(loc)) {
         RSign customLoc = this.getSign(loc);
         this.signs.remove(customLoc);
         return true;
      } else {
         return false;
      }
   }

   public RSign createSign(Location loc, int blocks, int price, int cooldown, World w) {
      int x = loc.getBlockX();
      int y = loc.getBlockY();
      int z = loc.getBlockZ();
      String world = loc.getWorld().getName();
      return new RSign(x, y, z, world, blocks, price, cooldown);
   }

   public String RSignToString(RSign rsign) {
      return rsign.x + "," + rsign.y + "," + rsign.z + "," + rsign.world + "," + rsign.blocks + "," + rsign.price + "," + rsign.cooldown;
   }

   public boolean setSignCooldown(Player p, Location loc, int cooldown) {
      loc = loc.getBlock().getLocation();
      if (this.isSign(loc)) {
         RSign customLoc = this.getSign(loc);
         customLoc.cooldown = cooldown;
         p.sendMessage("§7Cooldown has been set to " + cooldown + " second/s");
         return true;
      } else {
         p.sendMessage("§7That is not a RandomTP sign");
         return false;
      }
   }

   public RSign stringToRSign(String string) {
      String[] loc = string.split(",");
      int x = Integer.valueOf(loc[0]);
      int y = Integer.valueOf(loc[1]);
      int z = Integer.valueOf(loc[2]);
      String world = loc[3];
      int blocks = Integer.valueOf(loc[4]);
      int price = Integer.valueOf(loc[5]);
      int cooldown = Integer.valueOf(loc[6]);
      return new RSign(x, y, z, world, blocks, price, cooldown);
   }

   public boolean isNumeric(String str) {
      char[] var5;
      int var4 = (var5 = str.toCharArray()).length;

      for(int var3 = 0; var3 < var4; ++var3) {
         char c = var5[var3];
         if (!Character.isDigit(c)) {
            return false;
         }
      }

      return true;
   }

   public boolean canUseTeleport(Player p) {
      if (this.plugin.getDisabledWorlds().contains(p.getLocation().getWorld().getName())) {
         p.sendMessage(this.plugin.getLang().getConfig().getString("NOTALLOWEDINTHATWORLD").replace("&", "§"));
         return false;
      } else {
         return true;
      }
   }

   public List<Block> getNearbyBlocks(Location location, int radius) {
      List<Block> blocks = new ArrayList<>();

      for(int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; ++x) {
         for(int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; ++y) {
            for(int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; ++z) {
               blocks.add(location.getWorld().getBlockAt(x, y, z));
            }
         }
      }

      return blocks;
   }

   public void saveCooldowns() {
      List<UUID> lfs = new ArrayList<>(this.cooldown.keySet());
      List<UUID> lfsg = new ArrayList<>(this.cooldowngui.keySet());
      List<UUID> lfsp = new ArrayList<>(this.cooldownportal.keySet());

      int i;
      UUID uuid;
      for(i = 0; i < lfs.size(); ++i) {
         uuid = (UUID)lfs.get(i);
         this.plugin.getDb().getConfig().set("Cooldowns." + uuid.toString(), this.cooldown.get(uuid));
      }

      for(i = 0; i < lfsg.size(); ++i) {
         uuid = (UUID)lfsg.get(i);
         this.plugin.getDb().getConfig().set("CooldownsGUI." + uuid.toString(), this.cooldowngui.get(uuid));
      }

      for(i = 0; i < lfsp.size(); ++i) {
         uuid = (UUID)lfsp.get(i);
         this.plugin.getDb().getConfig().set("CooldownsPortal." + uuid.toString(), this.cooldownportal.get(uuid));
      }

   }

   public void loadCooldowns() {
      Set<String> lists;
      ArrayList<String> lfs;
      int i;
      String suuid;
      UUID uuid;
      List<String> list;
      int i2;
      String s;
      String[] rportal;
      Long time_elapsed;
      if (this.plugin.getDb().getConfig().getString("Cooldowns") != null) {
         lists = this.plugin.getDb().getConfig().getConfigurationSection("Cooldowns").getKeys(false);
         lfs = new ArrayList<>(lists);

         for(i = 0; i < lfs.size(); ++i) {
            suuid = (String)lfs.get(i);
            uuid = UUID.fromString(suuid);
            list = this.plugin.getDb().getConfig().getStringList("Cooldowns." + suuid);

            for(i2 = 0; i2 < list.size(); ++i2) {
               s = (String)list.get(i2);
               rportal = s.split("-");
               time_elapsed = (System.currentTimeMillis() - Long.valueOf(rportal[0])) / 1000L;

               try {
                  RSign cartel = this.plugin.getSignUtil().stringToRSign(rportal[1]);
                  if (time_elapsed >= (long)cartel.cooldown) {
                     list.remove(s);
                  }
               } catch (Exception var14) {
               }
            }

            this.plugin.getDb().getConfig().set("Cooldowns." + uuid.toString(), list);
            this.cooldown.put(uuid, list);
         }
      }

      if (this.plugin.getDb().getConfig().getString("CooldownsGUI") != null) {
         lists = this.plugin.getDb().getConfig().getConfigurationSection("CooldownsGUI").getKeys(false);
         lfs = new ArrayList<>(lists);

         for(i = 0; i < lfs.size(); ++i) {
            suuid = (String)lfs.get(i);
            uuid = UUID.fromString(suuid);
            list = this.plugin.getDb().getConfig().getStringList("CooldownsGUI." + suuid);

            for(i2 = 0; i2 < list.size(); ++i2) {
               s = (String)list.get(i2);
               rportal = s.split("-");
               time_elapsed = (System.currentTimeMillis() - Long.valueOf(rportal[0])) / 1000L;

               try {
                  GUIItem item = (GUIItem)this.plugin.getGui().gui.get(Integer.valueOf(rportal[1]));
                  if (time_elapsed >= (long)item.cooldown) {
                     list.remove(s);
                  }
               } catch (Exception var13) {
               }
            }

            this.plugin.getDb().getConfig().set("CooldownsGUI." + uuid.toString(), list);
            this.cooldowngui.put(uuid, list);
         }
      }

      if (this.plugin.getDb().getConfig().getString("CooldownsPortal") != null) {
         lists = this.plugin.getDb().getConfig().getConfigurationSection("CooldownsPortal").getKeys(false);
         lfs = new ArrayList<>(lists);

         for(i = 0; i < lfs.size(); ++i) {
            suuid = (String)lfs.get(i);
            uuid = UUID.fromString(suuid);
            list = this.plugin.getDb().getConfig().getStringList("CooldownsPortal." + suuid);

            for(i2 = 0; i2 < list.size(); ++i2) {
               s = (String)list.get(i2);
               rportal = s.split("_");
               time_elapsed = (System.currentTimeMillis() - Long.valueOf(rportal[0])) / 1000L;

               try {
                  Integer id = Integer.valueOf(rportal[1]);
                  if (time_elapsed >= (long)((Portal)this.plugin.getPortals().get(id)).cooldown) {
                     list.remove(s);
                  }
               } catch (Exception var12) {
               }
            }

            this.plugin.getDb().getConfig().set("CooldownsPortal." + uuid.toString(), list);
            this.cooldownportal.put(uuid, list);
         }
      }

      this.saveDatabase();
   }
}
