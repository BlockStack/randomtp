package com.gmail.theposhogamer.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.theposhogamer.RandomTP;

public class GUI {
	RandomTP plugin;
	int rows;
	String menuname;
	HashMap<Integer, GUIItem> gui = new HashMap<>();

	public GUI(RandomTP plugin) {
		this.plugin = plugin;
	}

	public void openGUI(final Player p) {
		PrivateInventory inventory = new PrivateInventory(this.menuname, this.rows * 9, p.getUniqueId(),
				(ItemStack) null);
		final UUID uuid = p.getUniqueId();

		for (Integer slot : this.gui.keySet()) {
			GUIItem item = this.gui.get(slot);
			String[] lore = (String[]) item.lore.toArray(new String[item.lore.size()]);
			int id = 0;
			int data = 0;
			if (item.id.contains(":")) {
				String[] other = item.id.split(":");
				id = Integer.valueOf(other[0]);
				data = Integer.valueOf(other[1]);
			} else {
				id = Integer.valueOf(item.id);
			}

			inventory.setItem(new ItemStack(this.plugin.getMaterial(id, 0), 1, (byte) data), item.name,
					slot, new PrivateInventory.ClickRunnable() {
						@Override
						public void run(InventoryClickEvent e) {
							if (p.hasPermission(item.permission)) {
								if (!GUI.this.plugin.getSignUtil().canUseTeleport(p)) {
									return;
								}

								if (GUI.this.plugin.getSignUtil().cooldowngui
										.containsKey(uuid)) {
									List<String> cooldowns = GUI.this.plugin
											.getSignUtil().cooldowngui
													.get(uuid);

									for (int i = 0; i < cooldowns.size(); ++i) {
										String string = (String) cooldowns
												.get(i);
										String[] rsign = string.split("-");
										Long time_elapsed = System
												.currentTimeMillis()
												- Long.valueOf(rsign[0]);
										double seconds = (double) time_elapsed
												/ 1000.0D;
										if (Integer.valueOf(rsign[1]) == slot) {
											if (seconds < item.cooldown) {
												p.sendMessage(GUI.this.plugin
														.getLang()
														.getConfig()
														.getString("COOLDOWNLEFT")
														.replace("&", "§")
														.replace("%variable%",
																TimeUtils.calculateTime(
																		(long) (item.cooldown
																				- seconds))));
												return;
											}

											GUI.this.plugin
													.getSignUtil().cooldowngui
															.get(uuid).remove(
																	string);
										}
									}
								}

								if (GUI.this.plugin.getExternalReferences().vault) {
									try {
										double money = GUI.this.plugin
												.getEconomy()
												.getBalance(p);
										int cost = item.cost;
										if (cost != 0) {
											if (money < cost) {
												p.sendMessage(GUI.this.plugin
														.getLang()
														.getConfig()
														.getString("INSSUFICIENTMONEY")
														.replace("&", "§")
														.replace("%variable%",
																String.valueOf(cost)));
												return;
											}

											p.sendMessage(GUI.this.plugin
													.getLang()
													.getConfig()
													.getString("SUCCESSFULBUY")
													.replace("&", "§")
													.replace("%variable%",
															String.valueOf(cost)));
											GUI.this.plugin.getEconomy()
													.withdrawPlayer(p,
															cost);
										}
									} catch (Exception var9) {
										System.out.println(
												"RANDOMTP WARNING - You are using Vault but you don't have an economy plugin ");
									}
								}

								GUI.this.plugin.getPlayersInTeleportProcess().put(
										p.getUniqueId(),
										new TeleportTask(p, GUI.this.plugin,
												item.world,
												item.distance));
								if (item.cooldown != 0) {
									List<String> cds = new ArrayList<>();
									if (GUI.this.plugin.getSignUtil().cooldowngui
											.containsKey(uuid)) {
										cds = GUI.this.plugin
												.getSignUtil().cooldowngui
														.get(uuid);
									}

									cds.add(System.currentTimeMillis() + "-"
											+ slot);
									GUI.this.plugin.getSignUtil().cooldowngui
											.put(uuid, cds);
								}
							} else {
								p.sendMessage(GUI.this.plugin.getLang().getConfig()
										.getString("NOPERMISSIONGUIUSE")
										.replace("&", "§"));
							}

						}
					}, lore);

		}
		inventory.openInventory(p);
	}

	public void loadGUI() {
		this.menuname = this.plugin.getConfig().getString("Inventory.MenuName").replace("&", "§");
		this.rows = this.plugin.getConfig().getInt("Inventory.Rows");
		Iterator<String> var2 = this.plugin.getConfig().getConfigurationSection("Inventory.Items").getKeys(false)
				.iterator();

		while (var2.hasNext()) {
			String i = (String) var2.next();
			int cost = this.plugin.getConfig().getInt("Inventory.Items." + i + ".cost");
			int distance = this.plugin.getConfig().getInt("Inventory.Items." + i + ".distance");
			String id = this.plugin.getConfig().getString("Inventory.Items." + i + ".id").replace("&", "§")
					.replace("%cost%", String.valueOf(cost))
					.replace("%distance%", String.valueOf(distance));
			if (id == null) {
				id = "2";
			}

			int slot = this.plugin.getConfig().getInt("Inventory.Items." + i + ".slot");
			String name = this.plugin.getConfig().getString("Inventory.Items." + i + ".name")
					.replace("&", "§").replace("%cost%", String.valueOf(cost))
					.replace("%distance%", String.valueOf(distance));
			if (name == null) {
				name = "This is a name example";
			}

			String world = this.plugin.getConfig().getString("Inventory.Items." + i + ".world")
					.replace("&", "§").replace("%cost%", String.valueOf(cost))
					.replace("%distance%", String.valueOf(distance));
			if (world == null) {
				world = "world";
			}

			int cooldown = this.plugin.getConfig().getInt("Inventory.Items." + i + ".cooldown");
			String permission = this.plugin.getConfig().getString("Inventory.Items." + i + ".permission")
					.replace("&", "§").replace("%cost%", String.valueOf(cost))
					.replace("%distance%", String.valueOf(distance));
			if (permission == null) {
				permission = "randomtp.custompermission" + (new Random()).nextInt(999999);
			}

			List<String> lore = this.plugin.getConfig().getStringList("Inventory.Items." + i + ".lore");
			List<String> lorecolour = new ArrayList<>();

			for (int i2 = 0; i2 < lore.size(); ++i2) {
				String str = ((String) lore.get(i2)).replace("&", "§")
						.replace("%cost%", String.valueOf(cost))
						.replace("%distance%", String.valueOf(distance));
				lorecolour.add(str);
			}

			GUIItem item = new GUIItem(id, slot, name, cost, distance, world, cooldown, permission,
					lorecolour);
			this.gui.put(slot, item);
		}

	}
}
