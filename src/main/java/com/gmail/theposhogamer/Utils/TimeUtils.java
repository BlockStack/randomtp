package com.gmail.theposhogamer.Utils;

import java.util.concurrent.TimeUnit;

public class TimeUtils {
   public static String calculateTime(long seconds) {
      int day = (int)TimeUnit.SECONDS.toDays(seconds);
      long hours = TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours((long)day);
      long minute = TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.DAYS.toMinutes((long)day) - TimeUnit.HOURS.toMinutes(hours);
      long second = TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.DAYS.toSeconds((long)day) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minute);
      return day + "d " + hours + "h " + minute + "m " + second + "s";
   }

   public static String calculateMMSS(long seconds) {
      int day = (int)TimeUnit.SECONDS.toDays(seconds);
      long hours = TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours((long)day);
      long minute = TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.DAYS.toMinutes((long)day) - TimeUnit.HOURS.toMinutes(hours);
      long second = TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.DAYS.toSeconds((long)day) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minute);
      return minute + "m " + second + "s";
   }
}
