package com.gmail.theposhogamer.Utils;

public class XYCoord {
   public int x;
   public int y;

   public XYCoord(int x, int y) {
      this.x = x;
      this.y = y;
   }

   public boolean isSame(XYCoord othercoord) {
      return othercoord.x == this.x && othercoord.y == this.y;
   }
}
