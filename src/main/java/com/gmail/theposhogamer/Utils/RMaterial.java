package com.gmail.theposhogamer.Utils;

import java.util.EnumSet;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;

public class RMaterial {
   public Material getUpperMaterial(int ID, int data) {
      for(Material i : EnumSet.allOf(Material.class)) {
         if (i.getId() == ID) {
            return Bukkit.getUnsafe().fromLegacy(new MaterialData(i, (byte)data));
         }
      }

      return null;
   }

   public Material getMaterial(int typeId, int dataValue) {
      Material[] foundMaterial = new Material[]{null};
      EnumSet.allOf(Material.class).forEach((material) -> {
         if (material.getId() == typeId) {
            foundMaterial[0] = material;
         }

      });
      return foundMaterial[0];
   }
}
