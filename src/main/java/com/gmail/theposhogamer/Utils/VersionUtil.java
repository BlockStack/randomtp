package com.gmail.theposhogamer.Utils;

import com.gmail.theposhogamer.RandomTP;

public class VersionUtil {
   private VersionUtil.ServerVersion version;
   private String raw_version;

   public VersionUtil(RandomTP plugin) {
      String a = plugin.getServer().getClass().getPackage().getName();
      this.raw_version = a.substring(a.lastIndexOf(46) + 1);
      if (!this.raw_version.contains("v1_13") && !this.raw_version.contains("v1_14")) {
         this.version = VersionUtil.ServerVersion.ONE_SEVEN_TO_ONE_TWELVE;
      } else {
         this.version = VersionUtil.ServerVersion.ONE_THIRTEEN_AND_UP;
      }

   }

   public VersionUtil.ServerVersion getVersion() {
      return this.version;
   }

   public String getRawVersion() {
      return this.raw_version;
   }

   public static enum ServerVersion {
      ONE_SEVEN_TO_ONE_TWELVE,
      ONE_THIRTEEN_AND_UP;
   }
}
