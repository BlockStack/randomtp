package com.gmail.theposhogamer.Portals;

import org.bukkit.Location;

public class Portal {
   public int id;
   public Location up;
   public Location down;
   public Location middle;
   public double height;
   public String portalName;
   public String world;
   public int blocks;
   public int price;
   public int cooldown;
   public String permission;

   public Portal(String portalName, int id, Location up, Location down, String world, int blocks, int price, int cooldown, String permission) {
      this.portalName = portalName;
      this.id = id;
      this.up = up;
      this.down = down;
      this.world = world;
      this.blocks = blocks;
      this.price = price;
      this.cooldown = cooldown;
      this.permission = permission;
      if (up != null && down != null) {
         Location up_lowered;
         if (up.getY() < down.getY()) {
            up_lowered = down.clone();
            down = up.clone();
            up = up_lowered;
         }

         up_lowered = new Location(up.getWorld(), up.getX(), down.getY(), up.getZ());
         this.height = Math.sqrt(Math.pow(up.distance(down), 2.0D));
         this.middle = new Location(up.getWorld(), (down.getX() + up_lowered.getX()) / 2.0D, (down.getY() + up_lowered.getY()) / 2.0D, (down.getZ() + up_lowered.getZ()) / 2.0D);
      }

   }
}
