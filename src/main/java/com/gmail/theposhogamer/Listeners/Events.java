package com.gmail.theposhogamer.Listeners;

import com.gmail.theposhogamer.RandomTP;
import com.gmail.theposhogamer.Portals.Portal;
import com.gmail.theposhogamer.Utils.ExternalReferences;
import com.gmail.theposhogamer.Utils.RSign;
import com.gmail.theposhogamer.Utils.TeleportTask;
import com.gmail.theposhogamer.Utils.TimeUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Events implements Listener {
   private RandomTP plugin;

   public Events(RandomTP main) {
      this.plugin = main;
      this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
   }

   @EventHandler
   public void onInventoryClick(PlayerInteractEvent e) {
      Player p = e.getPlayer();
      ItemStack clicked;
      String dname;
      Block current;
      Location clickedBlockLoc;
      if (this.plugin.getPortalUtils().getPortalCreator().containsKey(p.getUniqueId())) {
         clicked = e.getItem();
         if (clicked != null && (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK)
               && clicked.hasItemMeta() && clicked.getItemMeta().hasDisplayName()) {
            dname = clicked.getItemMeta().getDisplayName();
            if (dname.equalsIgnoreCase("§c§lRTP §e- §6ϟ §cPortal Selection Tool §6ϟ")) {
               e.setCancelled(true);
               current = e.getClickedBlock();
               clickedBlockLoc = current.getLocation().clone();
               Portal portal = (Portal) this.plugin.getPortalUtils().getPortalCreator().get(p.getUniqueId());
               String position;
               if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                  position = "bottom";
                  portal.down = clickedBlockLoc.clone();
               } else {
                  position = "top";
                  portal.up = clickedBlockLoc.clone();
               }

               p.sendMessage(this.plugin.getPrefix() + "§aThe " + position + " corner has been set to §b" + "x:"
                     + clickedBlockLoc.getBlockX() + " y:" + clickedBlockLoc.getBlockY() + " z:"
                     + clickedBlockLoc.getBlockZ() + " §ain the world:§b " + clickedBlockLoc.getWorld().getName());
            }
         }
      }

      if (p.hasPermission("randomtp.portalcreator")) {
         clicked = e.getItem();
         if (clicked != null && e.getAction() == Action.RIGHT_CLICK_BLOCK && clicked.hasItemMeta()
               && clicked.getItemMeta().hasDisplayName()) {
            dname = clicked.getItemMeta().getDisplayName();
            if (dname.equalsIgnoreCase("§c§lRTP §e- §6ϟ §cPortal Eraser Tool §6ϟ")) {
               e.setCancelled(true);
               current = e.getClickedBlock();
               clickedBlockLoc = current.getLocation().clone();
               Portal portal = this.plugin.getPortalUtils().getPortalByNearBlocks(clickedBlockLoc);
               if (portal == null) {
                  p.sendMessage(this.plugin.getPrefix() + "§cThere's no near portals to remove");
               } else {
                  List<String> list = new ArrayList<>();
                  if (this.plugin.getDb().getConfig().getStringList("Portals") != null) {
                     list = this.plugin.getDb().getConfig().getStringList("Portals");
                  }

                  for (int i = 0; i < list.size(); ++i) {
                     String ps = list.get(i);
                     Portal oportal = this.plugin.getPortalUtils().stringToPortal(ps);
                     if (oportal.id == portal.id) {
                        list.remove(ps);
                     }
                  }

                  this.plugin.getDb().getConfig().set("Portals", list);
                  this.plugin.getDb().save();
                  Iterator<Block> var15 = this.plugin.getSignUtil().getNearbyBlocks(new Location(portal.up.getWorld(),
                        (portal.down.getX() + portal.up.getX()) / 2.0D, (portal.down.getY() + portal.up.getY()) / 2.0D,
                        (portal.down.getZ() + portal.up.getZ()) / 2.0D), (int) portal.height).iterator();

                  //TODO: What did the decompiler do here...
                  label66: while (true) {
                     Block b;
                     Material type;
                     do {
                        do {
                           if (!var15.hasNext()) {
                              this.plugin.getPortalUtils().set(portal.up, portal.down, Material.AIR);
                              p.sendMessage(this.plugin.getPrefix() + "§aPortal " + portal.id
                                    + " successfully removed from database");
                              this.plugin.getPortals().remove(portal.id);
                              break label66;
                           }

                           b = (Block) var15.next();
                        } while (this.plugin.getPortalUtils().isWaterInPortal(b));

                        type = b.getType();
                     } while (type != this.plugin.getMaterial(9, 0) && type != this.plugin.getMaterial(8, 0)
                           && type != this.plugin.getMaterial(36, 0));

                     b.setType(Material.AIR);
                     b.getState().update();
                  }
               }

               p.updateInventory();
            }
         }
      }

   }

   @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
   public void onBlockFromTo(BlockFromToEvent e) {
      Block b = e.getBlock();
      if ((b.getType() == this.plugin.getMaterial(9, 0) || b.getType() == this.plugin.getMaterial(8, 0))
            && this.plugin.getPortalUtils().isWaterInPortal(e.getBlock())) {
         e.setCancelled(true);
      }

   }

   @EventHandler
   public void onDamage(EntityDamageEvent e) {
      Entity ent = e.getEntity();
      if (ent instanceof Player
            && (e.getCause().equals(DamageCause.SUFFOCATION) || e.getCause().equals(DamageCause.FALL))) {
         Player p = (Player) ent;
         if (this.plugin.getPlayersInTeleportProcess().containsKey(p.getUniqueId())) {
            e.setCancelled(true);
            Block b = p.getLocation().getBlock();
            if (b.getRelative(BlockFace.UP).getType() != Material.AIR) {
               p.teleport(p.getLocation().add(0.0D, 1.0D, 0.0D).add(0.0D, 0.5D, 0.0D));
            }
         }
      }

   }

   @EventHandler
   private void onSignChange(SignChangeEvent e) {
      String line0 = e.getLine(0);
      if (line0.equalsIgnoreCase("[rtp]") || line0.equalsIgnoreCase("[randomtp]")) {
         Player p = e.getPlayer();
         if (p.hasPermission("randomtp.createsign")) {
            ExternalReferences extRef = this.plugin.getExternalReferences();
            Block b = e.getBlock();
            BlockState state = b.getState();
            Sign sign = (Sign) state;
            sign.update();
            int econprice = 0;
            int cooldown = 0;
            int blocks = 1000;
            String line1 = e.getLine(1);
            String line2 = e.getLine(2);
            String line3 = e.getLine(3);
            boolean line1empty = line1.isEmpty();
            boolean line2empty = line2.isEmpty();
            boolean line3empty = line3.isEmpty();
            if (!this.plugin.getSignUtil().isNumeric(line1)) {
               p.sendMessage(this.plugin.getLang().getMessage("NOTVALIDBLOCKNUMBER").replace("&", "§")
                     .replace("%variable%", line1));
               e.setCancelled(true);
               b.breakNaturally();
               return;
            }

            if (!line1empty) {
               blocks = Integer.valueOf(line1);
            } else {
               p.sendMessage(this.plugin.getLang().getMessage("EMPTYBLOCKNUMBER").replace("&", "§"));
               e.setCancelled(true);
               b.breakNaturally();
            }

            if (!this.plugin.getSignUtil().isNumeric(line2)) {
               p.sendMessage(this.plugin.getLang().getMessage("NOTVALIDPRICENUMBER").replace("&", "§")
                     .replace("%variable%", line1));
               e.setCancelled(true);
               b.breakNaturally();
               return;
            }

            if (!line2empty) {
               econprice = Integer.valueOf(line2);
            } else {
               p.sendMessage(this.plugin.getLang().getMessage("EMPTYPRICENUMBER").replace("&", "§"));
               e.setCancelled(true);
               b.breakNaturally();
            }

            if (line2empty) {
               econprice = 0;
               if (!extRef.vault) {
                  p.sendMessage(this.plugin.getLang().getMessage("ECONOMYNOTFOUND").replace("&", "§"));
               }
            } else if (!line2empty) {
               econprice = Integer.valueOf(line2);
            }

            World w = this.plugin.getServer().getWorld(line3);
            if (w == null) {
               p.sendMessage(this.plugin.getLang().getMessage("INVALIDWORLD").replace("&", "§"));
               e.setCancelled(true);
               b.breakNaturally();
               return;
            }

            if (line3empty) {
               p.sendMessage(this.plugin.getLang().getMessage("NOTVALIDPRICENUMBER").replace("&", "§")
                     .replace("%variable%", line1));
               e.setCancelled(true);
               b.breakNaturally();
            }

            RSign customLoc = this.plugin.getSignUtil().createSign(sign.getLocation(), blocks, econprice, cooldown, w);
            this.plugin.getSignUtil().getSigns().add(customLoc);
            List<String> lines = this.plugin.getConfig().getStringList("SignFormat");
            String distance = "%distance%";
            String price = "%price%";
            if (extRef.vault) {
               String stringPrice;
               if (econprice == 0) {
                  stringPrice = "";
               } else {
                  stringPrice = String.valueOf(econprice);
               }

               e.setLine(0,
                     ((String) lines.get(0)).replace("&", "§").replace(distance, line1).replace(price, stringPrice));
               e.setLine(1,
                     ((String) lines.get(1)).replace("&", "§").replace(distance, line1).replace(price, stringPrice));
               e.setLine(2,
                     ((String) lines.get(2)).replace("&", "§").replace(distance, line1).replace(price, stringPrice));
               e.setLine(3,
                     ((String) lines.get(3)).replace("&", "§").replace(distance, line1).replace(price, stringPrice));
            } else {
               e.setLine(0, ((String) lines.get(0)).replace("&", "§").replace(distance, line1).replace(price, ""));
               e.setLine(1, ((String) lines.get(1)).replace("&", "§").replace(distance, line1).replace(price, ""));
               e.setLine(2, ((String) lines.get(2)).replace("&", "§").replace(distance, line1).replace(price, ""));
               e.setLine(3, ((String) lines.get(3)).replace("&", "§").replace(distance, line1).replace(price, ""));
            }

            sign.update();
            this.plugin.getSignUtil().reloadSigns(false);
            this.plugin.getSignUtil().saveDatabase();
            this.plugin.getHelp().reload();
            p.sendMessage(this.plugin.getLang().getMessage("CREATINGSUCCESS").replace("&", "§"));
         }
      }

   }

   @EventHandler
   private void onSignDestroy(BlockBreakEvent e) {
      Block block = e.getBlock();
      if (block.getState() instanceof Sign) {
         Sign s = (Sign) block.getState();
         Location loc = s.getLocation();
         Player p = e.getPlayer();
         if (this.plugin.getSignUtil().isSign(loc)) {
            if (p.hasPermission("randomtp.breaksigns")) {
               this.plugin.getSignUtil().removeSign(loc);
               p.sendMessage(this.plugin.getLang().getConfig().getString("SIGNREMOVED").replace("&", "§"));
               this.plugin.getSignUtil().reloadSigns(false);
               this.plugin.getHelp().reload();
            } else {
               p.sendMessage(this.plugin.getLang().getConfig().getString("NOPERMISSION").replace("&", "§"));
               e.setCancelled(true);
            }
         }
      }

   }

   @EventHandler
   private void onPlayerInteract(PlayerInteractEvent e) {
      if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
         Block clicked = e.getClickedBlock();
         if (clicked.getState() instanceof Sign) {
            Sign s = (Sign) clicked.getState();
            Location loc = s.getLocation();
            if (this.plugin.getSignUtil().isSign(loc)) {
               Player p = e.getPlayer();
               if (!this.plugin.getSignUtil().canUseTeleport(p)) {
                  return;
               }

               UUID uuid = p.getUniqueId();
               if (p.hasPermission("randomtp.usesigns")) {
                  RSign sign = this.plugin.getSignUtil().getSign(loc);
                  if (this.plugin.getSignUtil().uuidCooldown.containsKey(uuid)) {
                     sign.cooldown = (Integer) this.plugin.getSignUtil().uuidCooldown.get(uuid);
                     p.sendMessage(
                           this.plugin.getPrefix() + "§aCooldown has been" + " set to " + sign.cooldown + " second/s");
                     this.plugin.getSignUtil().reloadSigns(false);
                     this.plugin.getSignUtil().saveDatabase();
                     this.plugin.getHelp().reload();
                     this.plugin.getSignUtil().uuidCooldown.remove(uuid);
                  } else {
                     if (this.plugin.getSignUtil().cooldown.containsKey(uuid)) {
                        List<String> cooldowns = this.plugin.getSignUtil().cooldown.get(uuid);

                        for (int i = 0; i < cooldowns.size(); ++i) {
                           String string = (String) cooldowns.get(i);
                           String[] rsign = string.split("-");
                           Long time_elapsed = System.currentTimeMillis() - Long.valueOf(rsign[0]);
                           RSign cartel = this.plugin.getSignUtil().stringToRSign(rsign[1]);
                           if (sign.x == cartel.x && sign.y == cartel.y && sign.z == cartel.z
                                 && sign.world.equalsIgnoreCase(cartel.world)) {
                              double seconds = (double) time_elapsed / 1000.0D;
                              if (seconds < (double) sign.cooldown) {
                                 p.sendMessage(this.plugin.getLang().getConfig().getString("COOLDOWNLEFT")
                                       .replace("&", "§").replace("%variable%",
                                             TimeUtils.calculateTime((long) ((double) sign.cooldown - seconds))));
                                 return;
                              }

                              this.plugin.getSignUtil().cooldown.get(uuid).remove(string);
                           }
                        }
                     }

                     if (this.plugin.getExternalReferences().vault) {
                        try {
                           double money = this.plugin.getEconomy().getBalance(p);
                           int cost = sign.price;
                           if (cost != 0) {
                              if (money < (double) cost) {
                                 p.sendMessage(this.plugin.getLang().getConfig().getString("INSSUFICIENTMONEY")
                                       .replace("&", "§").replace("%variable%", String.valueOf(cost)));
                                 return;
                              }

                              p.sendMessage(this.plugin.getLang().getConfig().getString("SUCCESSFULBUY")
                                    .replace("&", "§").replace("%variable%", String.valueOf(cost)));
                              this.plugin.getEconomy().withdrawPlayer(p, (double) cost);
                           }
                        } catch (Exception var16) {
                           this.plugin.log("§cYou are using Vault but you don't have an economy plugin");
                        }
                     }

                     this.plugin.getPlayersInTeleportProcess().put(uuid,
                           new TeleportTask(p, this.plugin, sign.world, sign.blocks));
                     if (sign.cooldown != 0) {
                        List<String> cooldowns = new ArrayList<>();
                        if (this.plugin.getSignUtil().cooldown.containsKey(uuid)) {
                           cooldowns = this.plugin.getSignUtil().cooldown.get(uuid);
                        }

                        cooldowns.add(System.currentTimeMillis() + "-" + this.plugin.getSignUtil().RSignToString(sign));
                        this.plugin.getSignUtil().cooldown.put(uuid, cooldowns);
                     }
                  }
               } else {
                  p.sendMessage(this.plugin.getLang().getConfig().getString("NOPERMISSION").replace("&", "§"));
               }
            }
         }

      }
   }
}
